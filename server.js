
/**
 * Module dependencies.
 */

var express = require('express')
  , process = require('process')
  , http = require('http')
  , bodyParser = require('body-parser')
  , request = require('request');

var app = express();
app.set('port', process.env.PORT || 3000);  
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/*FB bot code */
// for Facebook verification
app.get('/webhook', function(req,res){
    console.log("in GET");
    if (req.query['hub.verify_token'] === 'jewelfie_verify_token') {
        res.send(req.query['hub.challenge']);
    }
	console.log("in GET: Wrong token");
    res.send('Error, wrong token');	 
});

app.post('/webhook', function(req, res){
    console.log("in POST");
    var data = req.body;
    console.log(JSON.stringify(data));
    var messaging_events = req.body.entry[0].messaging;
    for (var i = 0; i < messaging_events.length; i++) {
        var event = req.body.entry[0].messaging[i];
        var sender = event.sender.id;
        if (event.message && event.message.text) {
            var text = event.message.text;
            console.log('Recieved text: ', text);
            if(text === 'Movie Merchandize'){
                sendGenericMessage(sender);
                continue;
            }
            if (text === 'Catalogs') {
                console.log("sending quick reply");
                sendQuickReply(sender);
                continue;
            }
            if (text === 'Generic') {
                console.log("sending generic");
                sendGenericMessage(sender);
                continue;
            }
            if(text === 'Menu') {
                addPersistentMenu();
            }
            sendTextMessage(sender, "Echo New: " + text.substring(0, 200));
        }
        if (event.postback) {
        text = JSON.stringify(event.postback);
        sendTextMessage(sender, "Postback received: "+text.substring(0, 200));
        continue;
        }
        if(event.option){
            console.log('opt in');
        }
    }
    res.sendStatus(200) ;
});

function sendTextMessage(sender, text) {
   	var messageData = { text:text };
    callSendAPI(sender,messageData);
}

function callSendAPI(sender, messageData) {   	
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
          access_token:"EAADBJwz912MBAMnKE3zZADKHtpbwHNQlKNuPZAYreP0Ny8srZBC70BbCOazmyMmGZCZB47X74n7HrUEUaEZCeHA2nnCDHGIAK8bJXCWHSEl4iFYRVFQHno1yyjgZC2HhXZBOe76BZCQjefZB8fcQ0eUmoWJn3EuQmbZABoXwJF34MX3eZAkyGdPNmnPu"
          },
        method: 'POST',
        json: {
            recipient: {id:sender},
            message: messageData,
        }
    }, function(error, response, body) {
        if (error) {
            console.log('Error sending messages: ', error);
        } else if (response.body.error) {
            console.log('Error: ', response.body.error);
        }
    });
}


function sendGenericMessage(sender) {
    var messageData = {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [{
                    "title": "BB Pendant",
                    "subtitle": "Movie merchandize",
                    "image_url": "http://jewelfieprod.blob.core.windows.net/tc-107/f8f382e5-73ff-49ac-9b92-144471d6ab35?lastModified=635714444141160000",
                    "buttons": [{
                        "type": "web_url",
                        "url": "http://www.silvostyle.com.com",
                        "title": "web url",
                    }, {
                        "type": "postback",
                        "title": "Buy",
                        "payload": "Buy - BB Pendant",
                    }],
                }, {
                    "title": "Heart in heart charm",
                    "subtitle": "Links of Life",
                    "image_url": "http://jewelfieprod.blob.core.windows.net/tc-107/4f8a7f25-5cf9-44ea-8970-8f52c6608b3e?lastModified=635658214186700000",
                    "buttons": [{
                        "type": "postback",
                        "title": "Buy",
                        "payload": "Buy - Heart in heart charm",
                    }],
                }]
            }
        }
    };
    callSendAPI(sender,messageData);
}

function sendQuickReply(sender) {
      var messageData= {
      text: "Here are our catalogs",
      quick_replies: [
        {
          "content_type":"text",
          "title":"Links of Life",
          "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_ACTION"
        },
        {
          "content_type":"text",
          "title":"Movie Merchandize",
          "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_COMEDY"
        },
        {
          "content_type":"text",
          "title":"Silver jewelry",
          "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_PICKING_DRAMA"
        }
      ]
    };
    callSendAPI(sender,messageData);
}    
  
function addPersistentMenu(){
 request({
    url: 'https://graph.facebook.com/v2.6/me/thread_settings',
    qs: { 
        access_token:"EAADBJwz912MBAFM8etzZCuVQSkNITUYfWPB5ao0mgPEC56nx9e1A0EBEsyKqKvBB8drmquknLgYULwCdioEwvZCCWbG0ZCTuhczW4swXd4lvFu2mA42yWyQggiLvsbA8s9vmCEByBlerBCZBn0kBkOPM03BtoBibKBDipzRmG3eZBWULdLgGo" 
        },
    method: 'POST',
    json:{
        setting_type : "call_to_actions",
        thread_state : "existing_thread",
        call_to_actions:[
            {
              type:"postback",
              title:"Home",
              payload:"Home"
            },
            {
              type:"postback",
              title:"Checkout",
              payload:"Checkout"
            },
            {
              type:"web_url",
              title:"Our Website",
              url:"http://www.silvostyle.com/"
            }
          ]
    }

}, function(error, response, body) {
    console.log(response);
    if (error) {
        console.log('Error sending messages: ', error);
    } else if (response.body.error) {
        console.log('Error: ', response.body.error);
    }
});

}
/*chat bot code ends*/

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
